package com.punicapp.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.punicapp.rxrepocore.LocalFilter;
import com.punicapp.rxrepocore.model.ListWrapper;
import com.punicapp.rxreporealm.InMemoryRepository;
import com.punicapp.rxreporealm.filters.InMemoryLocalFilter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "InMemorySample";

    public static class Cat {
        String name;
        int weight; // In kilograms
        Date birthDate;

        @SuppressWarnings("unused")
        public Cat() {
        }

        Cat(String name, int weight, Date birthDate) {
            this.name = name;
            this.weight = weight;
            this.birthDate = birthDate;
        }

        @Override
        public String toString() {
            return "Cat{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InMemoryRepository<Cat> catRepo = new InMemoryRepository<>(InMemoryRepository.SHALLOW_COPY_MODE);
        Observable.fromArray(
                new Cat("Vasya", 12, new Date(1999, 5, 17)),
                new Cat("Alisa", 2, new Date(2005, 2, 4)),
                new Cat("Murka", 4, new Date(2016, 8, 22)),
                new Cat("Qweqwe", 16, new Date(2001, 6, 3))
        ).doOnNext(catRepo.saveInChain()).subscribe();

        List<LocalFilter> filters = new ArrayList<>();
        filters.add(new InMemoryLocalFilter(Integer.class, LocalFilter.Check.Equal, "weight", 12));
        ListWrapper<Cat> cats = catRepo.instantFetch(filters, null);
        Log.d(TAG, "cats = " + cats.getData());
        cats.getData().get(0).name = "Pyotr";
        ListWrapper<Cat> cats1 = catRepo.instantFetch(filters, null);
        Log.d(TAG, "cats = " + cats.getData() + ", cats1 = " + cats1.getData());

        filters.clear();
        filters.add(new InMemoryLocalFilter(String.class, LocalFilter.Check.In, "name", new String[]{"Alisa", "Qweqwe"}));
        cats = catRepo.instantFetch(filters, null);
        Log.d(TAG, "cats 2 = " + cats.getData());

        filters.clear();
        filters.add(new InMemoryLocalFilter(String.class, LocalFilter.Check.GreatOrEqual, "birthDate", new Date(2004, 1, 1)));
        cats = catRepo.instantFetch(filters, null);
        Log.d(TAG, "cats 3 = " + cats.getData());
    }
}
