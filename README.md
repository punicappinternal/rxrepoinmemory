# RxRepo InMemory

[![](https://jitpack.io/v/org.bitbucket.punicappinternal/rxRepoInMemory.svg)](https://jitpack.io/#org.bitbucket.punicappinternal/rxRepoInMemory)

Punicapp InMemory repository, storing objects in RAM.

To install the library add: 
 
```
   allprojects {
       repositories {
           maven { url 'https://jitpack.io' }
       }
   }
   dependencies {
           compile 'org.bitbucket.punicappinternal:rxRepoInMemory:-SNAPSHOT'
   }
```
