package com.punicapp.rxreporealm;

import com.punicapp.rxrepocore.IRepository;
import com.punicapp.rxrepocore.LocalFilter;
import com.punicapp.rxrepocore.LocalSort;
import com.punicapp.rxrepocore.model.ListWrapper;
import com.punicapp.rxrepocore.model.Wrapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

public class InMemoryRepository<T> implements IRepository<T> {
    public static final int NO_COPY_MODE = 1;
    public static final int SHALLOW_COPY_MODE = 2;

    private Set<T> storage = Collections.synchronizedSet(new HashSet<T>());
    private int copyMode;

    public InMemoryRepository() {
        this(NO_COPY_MODE);
    }

    public InMemoryRepository(int copyMode) {
        if (!(copyMode == NO_COPY_MODE || copyMode == SHALLOW_COPY_MODE))
            throw new RuntimeException("Wrong copyMode " + copyMode);
        this.copyMode = copyMode;
    }

    @Override
    public Single<T> save(T item) {
        return Single.just(item).doOnSuccess(saveInChain());
    }

    @Override
    public Single<List<T>> saveAll(List<T> items) {
        return Single.just(items).doOnSuccess(saveAllInChain());
    }

    @Override
    public Consumer<T> saveInChain() {
        return new Consumer<T>() {
            @Override
            public void accept(T t) throws Exception {
                storage.add(t);
            }
        };
    }

    @Override
    public Consumer<List<T>> saveAllInChain() {
        return new Consumer<List<T>>() {
            @Override
            public void accept(List<T> ts) throws Exception {
                storage.addAll(ts);
            }
        };
    }

    @Override
    public Single<T> modifyFirst(Consumer<T> action) {
        return first(Collections.<LocalFilter>emptyList())
                .map(new Function<Wrapper<T>, T>() {
                    @Override
                    public T apply(@NonNull Wrapper<T> tWrapper) throws Exception {
                        if (tWrapper.isEmpty()) {
                            throw new IllegalStateException("Modified object is null!!!");
                        }
                        return tWrapper.getData();
                    }
                })
                .doOnSuccess(action)
                .doOnSuccess(saveInChain());
    }

    private Single<Collection<T>> getFiltered(final List<LocalFilter> filters) {
        return Single.fromCallable(new Callable<Collection<T>>() {
            @Override
            public Collection<T> call() throws Exception {
                return storage;
            }
        }).map(new Function<Collection<T>, Collection<T>>() {
            @Override
            public Collection<T> apply(Collection<T> ts) throws Exception {
                if (filters == null || filters.size() == 0) {
                    return ts;
                }
                for (LocalFilter filter : filters) {
                    ts = (Collection<T>) filter.applyFilter(ts);
                }
                return ts;
            }
        });
    }

    @Override
    public Single<Integer> removeInChain(List<LocalFilter> filters) {
        return getFiltered(filters).map(new Function<Collection<T>, Integer>() {
            @Override
            public Integer apply(Collection<T> ts) throws Exception {
                storage.removeAll(ts);
                return ts.size();
            }
        });
    }


    @Override
    public Single<ListWrapper<T>> fetch(List<LocalFilter> filters, LocalSort sort) {
        return getFiltered(filters).map(new Function<Collection<T>, ListWrapper<T>>() {
            @Override
            public ListWrapper<T> apply(Collection<T> ts) throws Exception {
                if (ts == null || ts.size() == 0) {
                    return new ListWrapper<>(null);
                }

                if (copyMode == NO_COPY_MODE)
                    return new ListWrapper<>(new ArrayList<>(ts));
                else {
                    ArrayList<T> list = new ArrayList<>(ts.size());
                    for (T obj : ts) {
                        list.add(ObjectCloner.cloneObject(obj));
                    }
                    return new ListWrapper<>(list);
                }
            }
        });
    }

    @Override
    public Single<Wrapper<T>> first(List<LocalFilter> filters) {
        return getFiltered(filters).map(new Function<Collection<T>, Wrapper<T>>() {
            @Override
            public Wrapper<T> apply(Collection<T> ts) throws Exception {
                Iterator<T> iterator = ts.iterator();
                if (!iterator.hasNext()) {
                    return new Wrapper<>(null);
                }
                T next = iterator.next();
                return new Wrapper<>(copyMode == NO_COPY_MODE ? next : ObjectCloner.cloneObject(next));
            }
        });
    }

    @Override
    public ListWrapper<T> instantFetch(List<LocalFilter> filters, LocalSort sort) {
        Single<ListWrapper<T>> initial = fetch(filters, sort);
        return initial.blockingGet();
    }

    @Override
    public Wrapper<T> instantFirst(List<LocalFilter> filters) {
        Single<Wrapper<T>> initial = first(filters);
        return initial.blockingGet();
    }

    @Override
    public Single<Long> count(final List<LocalFilter> filters) {
        return getFiltered(filters).map(new Function<Collection<T>, Long>() {
            @Override
            public Long apply(Collection<T> ts) throws Exception {
                return (long) ts.size();
            }
        });
    }

    @Override
    public Long instantCount(List<LocalFilter> filters) {
        Single<Long> initial = count(filters);
        return initial.blockingGet();
    }
}
