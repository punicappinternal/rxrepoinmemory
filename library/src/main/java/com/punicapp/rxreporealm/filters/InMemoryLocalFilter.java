package com.punicapp.rxreporealm.filters;

import com.punicapp.rxrepocore.LocalFilter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class InMemoryLocalFilter<U> extends LocalFilter<Collection<U>> {
    public InMemoryLocalFilter(Class clazz, Check check, String idProp, Object value) {
        super(clazz, check, idProp, value);
    }

    @Override
    public Collection<U> applyFilter(Collection<U> us) {
        if (us == null || us.size() == 0) {
            return null;
        }

        LocalFilter.Check check = getCheck();
        String prop = getIdProp();

        List<U> retList = null;
        try {
            Object object = us.iterator().next();
            Field field = object.getClass().getDeclaredField(prop);
            field.setAccessible(true);

            Object requestedValue = getValStr();

            retList = new ArrayList<>();

            List<Object> objList = null;
            if (check == Check.In)
                objList = Arrays.asList((Object[]) requestedValue);
            for (U item : us) {
                Object value = field.get(item);
                boolean checkResult = false;
                switch (check) {
                    case Equal:
                        checkResult = requestedValue.equals(value);
                        break;
                    case NotEqual:
                        checkResult = !requestedValue.equals(value);
                        break;
                    case GreatOrEqual:
                        if (value != null) {
                            int compared = ((Comparable) requestedValue).compareTo(value);
                            checkResult = compared <= 0;
                        }
                        break;
                    case isNull:
                        checkResult = value == null;
                        break;
                    case isNotNull:
                        checkResult = value != null;
                        break;
                    case In:
                        checkResult = objList.contains(value);
                        break;
                }

                if (checkResult) retList.add(item);
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return retList;
    }
}
