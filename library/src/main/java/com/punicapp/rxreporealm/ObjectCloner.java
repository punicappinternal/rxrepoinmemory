package com.punicapp.rxreporealm;

import android.util.Log;

import java.lang.reflect.Field;

class ObjectCloner {
    static <T> T cloneObject(T obj) {
        try {
            Class<?> clazz = obj.getClass();
            T clone = (T) clazz.newInstance();
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                field.set(clone, field.get(obj));
            }
            return clone;
        } catch (Exception e) {
            Log.w("Exception", e.toString(), e);
            return null;
        }
    }
}
